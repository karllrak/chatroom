#lang racket

;global....
(provide transferFile)
(define targetAddr "localhost")
(define targetPort 20000)
(define mtu 500); how much bytes to transfer each time

(struct exn:fail:network:timeout exn:fail:network ())
(define (transferFile filename)
  (define (sendFilePart data index)
;    (display (number->string index) out)
 ;   (display "/" out)
  ;  (display (number->string transferCount) out)
   ; (display ":" out) todo
    (write-bytes data out)
    (flush-output out))
  (define (sendFileEnds)
    (displayln "file ends sent")
    (define evt (sync/timeout 15 (read-line-evt in 'any)))
    (cond
     ((not evt) (raise (exn:fail:network:timeout) (current-continuation-marks)))
     ((string=? evt "get all!") (void))))

  ;todo check file exists and what?
  (define fileLen (file-size filename))
  (define-values (transferCount remainder) (quotient/remainder fileLen mtu))
  (cond ((not (= 0 remainder)) (set! transferCount (+ 1 transferCount))))
  
  (define-values (in out) (tcp-connect targetAddr targetPort))
  (displayln "target connected")
  (display (string-append "send you file " filename " with length "
                         (number->string fileLen) " mtu " (number->string mtu) " in total "
                         (number->string transferCount) "\n") out)
  (flush-output out)
  (displayln "file info sent")
  (define responseEvt (sync/timeout 10 (read-line-evt in 'any)))
  (cond ( (not responseEvt) (raise (current-continuation-marks))));todo raise what?
  (displayln "responseGet")

  (call-with-input-file
      filename
    (lambda (f) 
      (let sendFileLoop ([index 0])
        (define data (read-bytes mtu f));todo data may be wrong type
        (if (eof-object? data)
           (sendFileEnds)
           (begin
             (sendFilePart data index)
           ;  (displayln "file part sent once")
             (sendFileLoop (+ index 1))))))) ;todo may raise what?
  )  