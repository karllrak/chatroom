#lang racket
(provide dp dpf)

(define dp display)
(define dpf
  (lambda (p . l) (for ([i l]) (display i p)) (flush-output p)))
