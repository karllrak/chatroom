#lang racket

;todo struct with default here

(require racket/serialize)
(provide loadConfig saveConfig
         (struct-out localConfig))

(serializable-struct localConfig
                     (defaultHost))
(define (loadConfig)
  (with-handlers ([exn:fail? (lambda (e) (defaultConfig))])
                 (call-with-input-file "config"
                                       (lambda (in)
                                         (deserialize (port->string in))))))

(define (defaultConfig)
  (localConfig "localhost"))

(define (saveConfig)
  (call-with-output-file "config"
                         #:exists 'truncate
                         (lambda (out)
                           (write (serialize (defaultConfig)) out))))
