#lang racket

(provide userAdd getUserInfoByAddr
        (struct-out  exn:fail:UserExists)
        (struct-out exn:fail:NoSuchUser))

(struct exn:fail:UserExists exn ())
(struct exn:fail:NoSuchUser exn())

(define gUserDict (make-hash))

(define (userAdd name addr portNum)
  ;use remoteAddr remotePortNum from tcp-addresses to identify user
  (define userInfo (hash-ref gUserDict name #f))
  (if (not userInfo)
     (begin
       (hash-set! gUserDict name (list addr portNum))
       (hash-set! gUserDict (string-append addr (number->string portNum)) name))
     ;todo better way
     (raise (exn:fail:UserExists (string-append "username " 
                                               (if (symbol? name)  (symbol->string name) name) " already exists")
                                     (current-continuation-marks)))))

(define (getUserInfoByName name) #f)
(define (getUserInfoByAddr addr portNum) 
  ;todo parameter type check
  (define info (hash-ref gUserDict (string-append addr (number->string portNum)) #f))
  (if (not info)
     (exn:fail:NoSuchUser (string-append "no user from " addr " with port" (number->string portNum)) 
                         (current-continuation-marks))
     info)) 

(define (test)
  (userAdd 'k "127.0.0.1" 22)
  (userAdd 'b "22" 33)
  (display (getUserInfoByAddr "22" 33))
  (userAdd "2233" "no" 33)
  (userAdd 'k 11 33))