#lang racket
(require racket/date)
(provide newExecCmd )

(require "msgQueue.scm")
(require "user/identification.scm")

(define (logIt! act src dst data)
                  (display src)
                  (display #\space)
                  (display act)
                  (display (date->string (current-date) (date-display-format 'chinese)))
                  (display dst)
                  (display #\space)
                  (display data)
                  (display #\newline))

(define (errorSyntax s)
   (display "Wrong command syntax, with string:\n")
  (display s)
  (display #\newline))

(define (newExecCmd in out)
  (define src (username in out))
  (define cmdStr (getCmdStr in out))
  (define routeResult
    (for/or ([routePair gCmdParsingList])
      (define matchResult (regexp-match (car routePair) cmdStr))
      (if matchResult
       (apply (cadr routePair) (append matchResult (list src)))
       #f)))
    (if (not routeResult)
       (errorSyntax cmdStr)
       (void)))

(define (getMsgRoute originS src)
  (display src)
  (display " getting msg\n"))

(define (sendMsgRoute originS dst data src)
     (and (sendMsg src dst data) (logIt! 'sendMsg src dst data)))

(define gCmdParsingList
  `((#px"getMsg" ,getMsgRoute)
    (#px"to (.*):(.*)" , sendMsgRoute)
    ))

(define (getCmdStr in out)
   (define evt (sync/timeout 1 (read-line-evt in 'any)))
  (if (not evt)
     (getCmdStr in out)
     evt))