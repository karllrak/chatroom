#lang racket
(require racket/tcp)
(require "server/recentConnections.scm")
(require "debug.scm")

(define-syntax connectTargetServer
  (syntax-rules ()
    [(_ expr)
     (let-values ( ([addr portNum] expr) )
       (tcp-connect addr portNum))]))

(define (queryMsg in out)
  (displayln "getMsg" out)
  (flush-output out)
  )

(struct exn:fail:disconnected exn:fail:network ())
(define (chatNow in out)
  (let loop ()
    (define readEvt (sync/timeout 3.5 (read-line-evt in 'any)))
    (cond [(eof-object? readEvt) (raise (exn:fail:disconnected "eof read, the connection may be lost" (current-continuation-marks)))])
    (if (and readEvt (not (string=? readEvt "get it!")))
      (begin
        (displayln readEvt)
        (flush-output))
      (void))
    (define writeEvt (sync/timeout 2.5 (read-line-evt (current-input-port) 'any)))
    (if (not writeEvt)
      (queryMsg in out)
      (begin
        (displayln writeEvt out)
        (flush-output out)))
    (loop)))

(readRecentConnections)
(showRecentConnections)

(define choiceOrAddr void)
(if gDebug
  (set! choiceOrAddr (autoConnectRcntServers))
  (set! choiceOrAddr (read-line)))

(let-values ( ([in out]
               (cond
                 [(list? choiceOrAddr) (values (car choiceOrAddr) (cadr choiceOrAddr))]
                 [(string->number choiceOrAddr)
                  (and (<=  (string->number choiceOrAddr) (rcntConnectionCount));todo what if failed
                       (connectTargetServer (recentlyConnectedServers (string->number choiceOrAddr))))]
                 (else (begin
                         (addToRcnt choiceOrAddr)
                         (connectTargetServer (str->addrPort choiceOrAddr))))) ))
            (chatNow in out))

(error "connect failed")
