#lang racket

(provide usernameAllowed)

(define (usernameAllowed s)
  (and
   (not (eof-object? s))
   (not (string=? s "getMsg"))));todo more constraint
