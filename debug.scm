#lang racket

(provide gDebug)

(define gDebug (with-handlers*
                 ([exn:fail? (lambda (e) #f)])
                 (call-with-input-file
                   "Debug_on"
                   (lambda (f) #t))))

