#lang racket

(require "fileReceive.scm")
(require "fileTransfer.scm")

(define testFileName "test8.mp4")
(define recvThread (thread (lambda () (startRecving))))
(sleep 1)
(define sendThread (thread (lambda () (transferFile testFileName))))


(sleep 25)
(if (and (file-exists? (string->path (string-append testFileName "0"))) (equal? (file-size testFileName) (file-size (string-append testFileName "0"))))
    (void)
;    (begin (delete-file (string->path "requirements0")) (raise 'testFailed)))
    (raise 'testFailed))

#|
createReceivePorts
sendFileInfo
getFileInfo
sendFilePart
getFilePartAsync
|#
