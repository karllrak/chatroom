#lang racket

(provide startRecving)
;global variant first =_=
(define gFileRecvAddr "localhost")
(define gFileRecvPort 20000)

(define (readAllBytesFromPort p (startSize 1500)) 
    (define evt 'nosense)
    (let l ([count startSize])
      (if (count .  <  . 1)
         (make-bytes 0 0)
         (begin
           (set! evt (sync/timeout 0.05 (read-bytes-evt count p)))
           (if evt
              (bytes-append evt (l count))
              (l (quotient count 2)))))))
         
(define (startRecving)
  (define fileRecvListener (tcp-listen gFileRecvPort))
  (define-values (in out) (tcp-accept fileRecvListener))
  (displayln "startRecving")
  ;todo the wrong file
  ;todo multi file recv
  (define infoEvt (sync/timeout 10 (read-line-evt in 'any)))
  (define-values (filename fileLen mtu totalCount)
    (values void void void void))
  
  
  (cond ((not infoEvt) (raise (current-continuation-marks)))
         ((string? infoEvt) 
          (begin 
            (define fileInfo
              (regexp-match #px"send you file (.*) with length (.*) mtu (.*) in total (.*)"  infoEvt))
            (set!-values (filename fileLen mtu totalCount)
                         (values (cadr fileInfo)
                                 (string->number(caddr fileInfo)) 
                                 (string->number (cadddr fileInfo))
                                 (string->number (list-ref fileInfo 4))))
            (displayln "file info get"))))
  (display "get it\n" out)
  (flush-output out)
  (displayln "response sent")
  
  (define (newFilename s) (string-append s "0"));todo wtf!
  (let chooseFileNameLoop ()
    (if (file-exists? (string->path filename))
        (begin 
          (set! filename (newFilename filename))
          (chooseFileNameLoop))
        (void)))
  (displayln (string-append "choose filename " filename))
  (define (getFilePart)
    (define evt 'no)
    (let soWhatIsThisLoop? ()
  ;    (displayln "before read-bytes-evt")
      (set! evt (sync/timeout 1 (read-bytes-evt mtu in)))
   ;   (displayln "filePart get once")
      (cond [(not evt) (readAllBytesFromPort in)];todo what if file is transfering
            [else evt])))
  
  (define (sendRecvEnds)
    (displayln "get all!" out)
    (flush-output out))
  
  (call-with-output-file filename
    (lambda (f)
      (define filePart 3)
      (let writeFileLoop ([curCount 0])
        (if (curCount . < . totalCount)
            (begin (set! filePart (getFilePart)) (display filePart f) 
                  (if ((bytes-length filePart) . <  . mtu)
                     (sendRecvEnds)
                     (writeFileLoop (+ curCount 1))))
            (void)))))
)