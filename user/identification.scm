#lang racket

(require "../user/session.scm")
(require "../user/nameCheck.scm")
(require "../tools/displayWithFlush.scm");todo unable to test relative import
(provide identifyUser
        username)

(define NEW_NAME_MAX_RETRY 3)

(define (closeConnection) #f)
  
(define (username in out)
  (define-values (a b remoteAddr remotePortNum) (tcp-addresses in #t))
  (getUserInfoByAddr remoteAddr remotePortNum))

(define (identifyUser in out)
  (dpf out "Who are you?\n")
  (define-values (localAddr localPortNum remoteAddr remotePortNum) (tcp-addresses in #t))     
  (define (onProperUsernameGet name out)
    (begin
      (dpf out "welcome! " name "\n" )
      (userAdd name remoteAddr remotePortNum));todo check name
    )
  (define (properNameRequest s)
    (define newNameEvt void)
    (if (usernameAllowed s)
       (begin
         (onProperUsernameGet s out) s)
       (begin
        ; (dpf out "getMsg is not a good name! Enter another one..\n")
         (let newNameRetryLoop ([retry 0])
           (if (< retry NEW_NAME_MAX_RETRY)
              (begin
                (set! newNameEvt (sync/timeout 3 (read-line-evt in 'any)))
                (if (not newNameEvt)
                   (newNameRetryLoop (+ retry 1))
                   (properNameRequest newNameEvt)))
              (closeConnection))))
       ))
  
  ;now read line
  (define evt (sync/timeout 2 (read-line-evt in 'any)))
  (if (not evt)
     (closeConnection)
     (properNameRequest evt)))
