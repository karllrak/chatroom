#lang racket

(require data/queue)
(provide make-queue enqueue! gMsgDict sendMsg getMsg
        (struct-out Cmd))
(define-struct Cmd (act target data))

(define gMsgDict (make-hash)) ;dict of list
;list is '(data data '(dst data))
  
(define sendMsg
  (case-lambda
    ([target data]
  ;todo too slow here and see HashTables doc for concurrent
     (define targetList (hash-ref gMsgDict target #f))
     (if targetList
        (set! targetList (append targetList (list data)))
        (set! targetList (list data)))
     (hash-set! gMsgDict target targetList))
    ([src dst data]
     (define targetList (hash-ref gMsgDict dst #f))
     (if targetList
        (set! targetList (append targetList `((,src ,data))))
        (set! targetList `((,src ,data))))
     (hash-set! gMsgDict dst targetList))))

(define (getMsg name)
  (define targetList (hash-ref gMsgDict name #f))
  (if (and targetList (not (empty? targetList)))
     (begin0
       (first targetList)
       (hash-set! gMsgDict name (rest targetList)))
     eof))

(define (test)
  (sendMsg 'k "a")
  (getMsg 'k)
  (getMsg 'l)
  (sendMsg "k" 'a)
  (getMsg "k")
  (getMsg "k")
  (getMsg 'nosuch)
  (sendMsg 'b 'b)
  (sendMsg 'b 'c)
  (sendMsg 'b 'd)
  (getMsg 'b)
  )



