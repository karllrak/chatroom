#lang racket

(provide echo)
(define echo
  (lambda l
    (for ([i l])
         (display i))
    (display "\n")))
