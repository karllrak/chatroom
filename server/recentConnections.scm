#lang racket

(provide gRcntConList gRcntConFile rcntConnectionCount addToRcnt writeRcntServers 
        test3
        readRecentConnections showRecentConnections recentlyConnectedServers
        str->addrPort autoConnectRcntServers )

(define gRcntConList '())
(define gRcntConFile "server/rcntConnections")
(define (rcntConnectionCount) (length gRcntConList))

(define (autoConnectRcntServers)
  ;return (list tcp-in-port tcp-out)  or string, should we use contract?
  (call/cc (lambda (k)
             (for ([i gRcntConList])
               (with-handlers* ([exn:fail? (lambda (e) #t)])
                              (define-values (addr port) (str->addrPort i))
                              (define-values (in out) (tcp-connect addr port))
                              (k (list in out))))
             ;so they all failed
             (displayln "auto connect recently server failed... Enter it plz")
             (k (read-line)))))
  
(define (recentlyConnectedServers choiceOrAddr)
  (str->addrPort (list-ref gRcntConList choiceOrAddr)))

(define (str->addrPort s)
  (define r (regexp-match #px"(.*) (.*)" s))
  (if (not r)
     (error (string-append s " is not valid address and port"))
     (values (cadr r) (string->number (caddr r)))))

(define (showRecentConnections)
  (let show ([l gRcntConList] [index 0])
    (if (empty? l)
       (void)
       (begin
         (display index)
         (display #\space)
         (displayln (car l))
         (show (cdr l) (+ index 1))))))
  
(define (readRecentConnections)
  (with-handlers*
   ([exn:fail? (lambda (e) #f)])
   (call-with-input-file
       gRcntConFile
     (lambda (f)
       (let loop ()
         (define l (read-line f))
         (if (eof-object? l)
            (void)
            (begin
              (set! gRcntConList (append gRcntConList (list l)))
              (loop))))))))
              
(define (writeRcntServers)
  (with-handlers* ([exn:fail? (lambda (e) (void))]) ;todo really good?
                 (call-with-output-file
                     gRcntConFile
                   #:exists 'truncate
                   (lambda (f)
                     (for ([i gRcntConList])
                       (displayln i f))))))

(define (addToRcnt s)
  (if (member s gRcntConList)
     (void)
     (begin
       (set! gRcntConList (append (list s) gRcntConList))
       (writeRcntServers))))

(define (test1)
  (set! gRcntConList '("127.0.0.1 12222" "127.0.0.1 23333"))
  (autoConnectRcntServers))
(define (test2)
    (set! gRcntConList '("127.0.0.1 10000" "127.0.0.1 23333"))
  (autoConnectRcntServers))
(define (test3)
    (set! gRcntConList '("127.0.0.1 12222" "127.0.0.1 10000"))
  (autoConnectRcntServers))

