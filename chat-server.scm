#lang racket
(require racket/tcp)
(require "debug.scm")
(require "echo.scm")
(require "msgQueue.scm")
(require "execCommand.scm")
(require "user/session.scm")
(require "user/identification.scm")
(require "tools/displayWithFlush.scm")

(define g_max_bind_retry 30)
(define g_listen_port 10000)
(define listener 'nonsense)

(let bindPort ([retry 0])
  (with-handlers*    ((exn:fail:network?
                       (lambda (e)  (cond
                                      ((= retry 0) (display "retry.")
                                       (sleep 1) (bindPort (+ retry 1)))
                                      ((< retry g_max_bind_retry)
                                       (begin  (display retry )  (sleep 1) (bindPort (+ retry 1))))
                                      (else(echo "failed binding after " g_max_bind_retry "times"));todo exit
           ))))
     (set! listener
       (tcp-listen (+ g_listen_port retry)))))

(define (userOnLine) ;todo
  #t)

(define (startChatting in out)
  (let chatLoop ()
  (when (userOnLine)
    (newExecCmd in out)
    (define msg (getMsg (username in out)))
    (cond ((list? msg) (set! msg (string-append (car msg) ": " (cadr msg)))));  karllrak: missU!
    (if (eof-object? msg)
       (dpf out "get it!\n" )
       (dpf out msg #\newline))
          ;exec command & change state
    )
    (chatLoop)
    (closeListener in out)))
(define (closeListener in out)
  (displayln "Bye bye!" out)
  (close-input-port in)
  (close-output-port out))

(define myTcpAcceptListener
  (lambda (in-out)
    (let ([in (car in-out)]
          [out (second in-out)])
    (if (identifyUser in out)
      (startChatting  in out)
      (closeListener in out)))))

(echo "start serving...")

(define (listenProc listener)
  (define-values (in out) (tcp-accept listener))
  (myTcpAcceptListener (list in out)))

(define (serverLoop)
  (with-handlers*
    ([exn:break? (lambda (e)
                   (if (eq? 'y
                            (begin (displayln "quit server? y/[n]")
                                  (if gDebug 'y (read))))
                      (void)
                      (serverLoop)))])
    (sync (handle-evt (tcp-accept-evt listener) (lambda (l) (thread (lambda () (myTcpAcceptListener l))))))
    (sleep 0.005);todo better way than sleep? Do we need sleep after sync?
    (serverLoop)))

(serverLoop)

(tcp-close listener)
(custodian-shutdown-all (current-custodian))
(displayln "I have closed the server!")
